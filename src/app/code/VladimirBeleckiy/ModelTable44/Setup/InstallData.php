<?php

namespace VladimirBeleckiy\ModelTable\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        /**
         * Install messages
         */
        $setup->startSetup();

        $data = [
            ['name' => '111 HappyYear'],
            ['name' => '222 Merryistmas']
        ];
        foreach ($data as $bind) {
            $setup->getConnection()
                ->insertForce($setup->getTable('model_table'), $bind);
        }

        $setup->endSetup();
    }
}