<?php
namespace VladimirBeleckiy\ModelTable\Model\ResourceModel\Post;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'post_id';
    protected $_eventPrefix = 'office_staff_post_collection';
    protected $_eventObject = 'post_collection';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('VladimirBeleckiy\ModelTable\Model\Post', 'VladimirBeleckiy\ModelTable\Model\ResourceModel\Post');
    }

}
