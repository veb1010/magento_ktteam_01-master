<?php

namespace VladimirBeleckiy\TableTrainingg\Setup;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;


class InstallSchema implements InstallSchemaInterface
{

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        /**
         * Create table 'TableTraining'
         */

        $setup->startSetup();
        $table = $setup->getConnection()
            ->newTable($setup->getTable('table_trainingg'))
            ->addColumn(
                'greeting_id',
                \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'Greeting ID'
            )
            ->addColumn(
                'message',
                \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                255,
                ['nullable' => false, 'default' => ''],
                'Message'
            )->setComment("table_trainingg table");
        $setup->getConnection()->createTable($table);

        $setup->endSetup();
    }
}