<?php

namespace VladimirBeleckiy\TableTrainingg\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{

    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        /**
         * Install messages
         */
        $setup->startSetup();

        $data = [
            ['message' => 'Happy New Year'],
            ['message' => 'Merry Christmas']
        ];
        foreach ($data as $bind) {
            $setup->getConnection()
                ->insertForce($setup->getTable('table_trainingg'), $bind);
        }

        $setup->endSetup();
    }
}