<?php

namespace VladimirBeleckiy\task471\Model\Resource\News;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
class Collection extends AbstractCollection
{
    /**
     * Define model & resource model
     */
    protected function _construct()
    {
        $this->_init(
            'VladimirBeleckiy\task471\Model\News',
            'VladimirBeleckiy\task471\Model\Resource\News'
        );
    }
}