<?php

namespace VladimirBeleckiy\task471\Model;

use Magento\Framework\Model\AbstractModel;

class News extends AbstractModel
{
    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init('VladimirBeleckiy\task471\Model\Resource\News');
    }
}