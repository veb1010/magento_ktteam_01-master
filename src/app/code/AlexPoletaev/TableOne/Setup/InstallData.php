<?php


namespace VledimirBeleckiy\TableOne\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;


class InstallData implements InstallDataInterface
{


    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {

        $data = [
            ['name' => 'Sergey'],
            ['name' => 'Dima'],
            ['name' => 'Alex'],
            ['name' => 'Madzor']
        ];
        foreach ($data as $bind) {
            $setup->getConnection()
                ->insertForce($setup->getTable('office_staff'), $bind);
        }
    }
}