<?php

namespace VladimirBeleckiy\TableOne\Setup;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;


class InstallSchema implements InstallSchemaInterface
{

    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        /**
         * Create table 'office_staff'
         */
        $installer = $setup;
        $installer->startSetup();

            $table = $installer->getConnection()->newTable(
                $installer->getTable('office_staff')
            )
                ->addColumn(
                    'employee2_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                    'Employee_ED'
                )
                ->addColumn(
                    'employee2_name',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                    255,
                    ['nullable' => false, 'default' => ''],
                    'Employee_Name'
                )->setComment("Office staff table");
            $installer->getConnection()->createTable($table);

        $installer->endSetup();
    }
}