<?php
namespace VladimirBeleckiy\TableOne\Model;
class Post extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'office_staff';

    protected $_cacheTag = 'office_staff';

    protected $_eventPrefix = 'office_staff';

    protected function _construct()
    {
        $this->_init('VladimirBeleckiy\TableOne\Model\ResourceModel\Post');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
}